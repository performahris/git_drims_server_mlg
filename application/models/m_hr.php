<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
Class M_hr extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function insert_departement($data){
		$this->db->insert('departement', $data);
	}
	function update($data, $id){
		$this->db->where('idbarang', $id);
		$this->db->update('barang', $data);
		die('tes');
	}
	function get_departement($id){
		$data = array();
		$this->db->select('*');
		$this->db->from('departement');
		$this->db->where('departement_id', $id);
		$hasil = $this->db->get();
		if($hasil->num_rows() > 0){
			return $hasil->row();
		}else{
			return $data;
		}
	}
	
	function update_departement($data, $id){
		$this->db->where('departement_id', $id);
		$this->db->update('departement', $data);
		die('tes');
	}

	function get_all_employee(){
		$this->db->select('*');
		$this->db->from('employee');
		$hasil = $this->db->get();
		return $hasil->result();
	}
	
	function departement_list(){
		$this->db->select('*');
		$this->db->from('departement');
		$hasil = $this->db->get();
		return $hasil->result();
	}
	
	function search_departement_list($que){
		
		$hasil = $this->db->query($que);
		return $hasil->result();
	}

	
}

/* End of file absen_m.php */
/* Location: ./application/models/absen_m.php */